package Negocio;



public class SopaDeLetras
{
    private char sopas[][];
    public int filaMatrizSopas, columMatrizSopas,numMayorLetras,numMenorLetras;
    boolean dispersa;
    
    public SopaDeLetras()
    {
        
    }

    
    
    
    
    
    public SopaDeLetras(String palabras) throws Exception
    {

        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        String palabras2[]=palabras.split(",");
        this.sopas=new char[palabras2.length][];
        int i=0;
        int c=0,c2=0;
        filaMatrizSopas=palabras2.length;
        for(String palabraX:palabras2)
        {
            this.sopas[i]=new char[palabraX.length()];
            pasar(palabraX,this.sopas[i]);
            i++;

            if(palabraX.length()>c)
            {
                c=palabraX.length();
            } 
            else
            {
                c2=palabraX.length();
            }

        }
        columMatrizSopas=c;
        numMayorLetras=c;
        numMenorLetras=c2;

        if(numMayorLetras!=numMenorLetras)
        {
            dispersa=true;
        }
        else
        {
            dispersa=false;
        }
    }
    
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopas)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
     public boolean esDispersa()
    {
        if(dispersa==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }  
     
    public boolean esCuadrada()
    {

        int filas=filaMatrizSopas;
        int colum=columMatrizSopas;

        if (esDispersa()==true)
            return false;

        if(filas==colum)
        {
            return true;
        }
        else{
            return false;
        }
    }
    

    
    public boolean esRectangular()
    {
        int f1=filaMatrizSopas;
        int c1=columMatrizSopas;

        if (esDispersa()==true)
            return false;

        if(c1>f1 || f1>c1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }  
    
    
    /*
        retorna cuantas veces esta la palabra en la matriz
       */
    public int getContar(String palabras/*, char letras[]*/)
    {
        char k[]=palabras.toCharArray();
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                System.out.println(k);
                
            }
        }
         
        
        return 0;
    }
    public char[] getDerechaIzquierda(char a[],String Palabra){
        
        for (int i = 0; i < sopas.length; i++) {
            
            
        }
        
        
        
        return 0;
    }
    
    /*
        debe ser cuadrada sopas
       */
    public char [] getDiagonalPrincipal() throws Exception
    {

        if(esCuadrada()==false){
            throw new Exception("Error no se puede crear la diagonal principal para la sopa de letras");

        }
        
        else{
            char f[]=new char[sopas.length];
            for (int i = 0; i < sopas.length; i++) {

                f[i]=sopas[i][i];

            }

            return  f;

        }
    }
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
